﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ModpackUpdate
{
    public class FileIO
    {
        //Initialize Directory strings
        public static string LocalD = Directory.GetCurrentDirectory();
        public static string LocalDD = Path.Combine(LocalD, "Data");
        public static string LocalDM = Path.Combine(LocalDD, "Mods");
        public static string LocalDC = Path.Combine(LocalDD, "Config");
        public static string LocalDP = Path.Combine(LocalDD, "Packs");
        public static string LocalDI = Path.Combine(LocalDD, "Installs");
        public static string MineDIR = "";
        public static string ConfURL = "";
        //Init DIRs

        public static void InitDirs()
        {
            //Load Directory Strings to string array
            string[] DirInit ={
                LocalD,
                LocalDD,
                LocalDM,
                LocalDC,
                LocalDP,
                LocalDI
            };
            //Make directories from array
            foreach (string i in DirInit)
            {
                Directory.CreateDirectory(i);
            }
        }

        //Reads from modpack mod list file
        public static void ReadListDL()
        {
            List<string> ls = new List<string>();
            
            if (URLInput.LinkType == "File")
            {
                string[] ss = File.ReadAllLines(DLMan.DLURL);
                foreach (string i in ss)
                {
                    ls.Add(i);
                }
                RMFiles();
                DLMan.DLMods(ls);
                MoveFiles();
            }
            else if(URLInput.LinkType == "URL")
            {
                DLMan.DLMPack(DLMan.DLURL);
                string[] ss = File.ReadAllLines(Path.Combine(LocalDP, DLMan.FileName));
                foreach (string i in ss)
                {
                    ls.Add(i);
                }
                RMFiles();
                DLMan.DLMods(ls);
                MoveFiles();
            }
        }

        public static void RMFiles()
        {
            string[] LLMods = Directory.GetFiles(LocalDM, "*.litemod");
            string[] FMods = Directory.GetFiles(LocalDM, "*.jar");
            string[] Confs = Directory.GetFiles(LocalDC);
            foreach(string file in LLMods)
            {
                File.Delete(file);
            }
            foreach (string file in FMods)
            {
                File.Delete(file);
            }
            foreach (string file in Confs)
            {
                File.Delete(file);
            }
        }
        //Remove DIR func
        private static void DeleteDirectory(string path)
        {
            if (Directory.Exists(path))
            {
                //Delete all files from the Directory
                foreach (string file in Directory.GetFiles(path))
                {
                    File.Delete(file);
                }
                //Delete all child Directories
                foreach (string directory in Directory.GetDirectories(path))
                {
                    DeleteDirectory(directory);
                }
                //Delete a Directory
                Directory.Delete(path);
            }
        }
        //Install mods
        public static void MoveFiles()
        {
            //Init vars for function
            string MInstallDIR = Path.Combine(MineDIR, "mods");
            string CInstallDIR = Path.Combine(MineDIR, "config");
            //Get Downloaded mods
            string[] LLMods = Directory.GetFiles(LocalDM, "*.litemod");
            string[] FMods = Directory.GetFiles(LocalDM, "*.jar");
            string[] Confs = Directory.GetFiles(LocalDC);
            //If more folders need init, add to this array
            string[] DirList =
            {
                "mods",
                "config"
            };
            //Create DIRs in minecraft from array
            foreach (string i in DirList)
            {
                if (Directory.Exists(Path.Combine(MineDIR, i)) == true)
                {
                    if (i != "config")
                    { 
                        Err.EM("Mod folder exists in minecraft folder, old mods will be Deleted", "Warning");
                        DeleteDirectory(Path.Combine(MineDIR, i));
                        Directory.CreateDirectory(Path.Combine(MineDIR, i));
                    }
                    else if (i == "config")
                    {
                        //Just here incase I need a catch for config
                    }
                }
                else
                {
                    Directory.CreateDirectory(Path.Combine(MineDIR, i));
                }
            }
            //Check that lists are populated
            if (Err.IsNonNullArray(FMods) || Err.IsNonNullArray(LLMods))
            {
                //Get file name of mod and move if not in folder
                foreach (string i in FMods)
                {
                    string LocalTarg = i;
                    string ModFile = Path.GetFileName(i);
                    if (Err.IsFileExists(Path.Combine(MInstallDIR, ModFile)) != true)
                    {
                        File.Copy(i, Path.Combine(MInstallDIR, ModFile));
                    }
                }
                foreach (string i in LLMods)
                {
                    string LocalTarg = i;
                    string ModFile = Path.GetFileName(i);
                    if (Err.IsFileExists(Path.Combine(MInstallDIR, ModFile)) != true)
                    {
                        File.Copy(LocalTarg, Path.Combine(MInstallDIR, ModFile));
                    }
                }
                if (Confs != null)
                {
                    foreach (string i in Confs)
                    {
                        string LocalTarg = i;
                        string ConfFile = Path.GetFileName(i);
                        if (Err.IsFileExists(Path.Combine(CInstallDIR, ConfFile)) != true)
                        {
                            File.Copy(LocalTarg, Path.Combine(CInstallDIR, ConfFile));
                        }
                    }
                }
            }
            //Return error if no files in list
            else
            {
                Err.EM("Lists are not populated for mod file, either the modlist is blank or something went horribly wrong\n Contact your modpack author","FILEIO ERROR");
            }
        }
    }
}
