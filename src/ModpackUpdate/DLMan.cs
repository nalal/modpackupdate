﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ModpackUpdate
{
    class DLMan
    {
        //Init vars for downloading
        public static string DLURL = "";
        public static string DLTarg = "";
        public static string DLLoc = "";
        public static string FileName = "";
        //Download mods to folder
        public static void DLMods(List<string> list)
        {
            if (Err.IsNonNullList(list))
            {
                foreach (string i in list)
                {
                    DLTarg = i;
                    FileName = System.IO.Path.GetFileName(DLTarg);
                    Form dl = new DLProgress();
                    dl.ShowDialog();
                }
            }
        }
        public static void DLMPack(string i)
        {
            DLTarg = i;
            FileName = System.IO.Path.GetFileName(DLTarg);
            Form dl = new DLProgress();
            dl.ShowDialog();
        }
    }
}
