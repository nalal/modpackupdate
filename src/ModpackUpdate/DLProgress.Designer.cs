﻿namespace ModpackUpdate
{
    partial class DLProgress
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PB = new System.Windows.Forms.ProgressBar();
            this.LDL = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // PB
            // 
            this.PB.Location = new System.Drawing.Point(12, 65);
            this.PB.Name = "PB";
            this.PB.Size = new System.Drawing.Size(386, 34);
            this.PB.TabIndex = 0;
            // 
            // LDL
            // 
            this.LDL.AutoSize = true;
            this.LDL.Location = new System.Drawing.Point(12, 9);
            this.LDL.Name = "LDL";
            this.LDL.Size = new System.Drawing.Size(81, 13);
            this.LDL.TabIndex = 1;
            this.LDL.Text = "DownloadLabel";
            // 
            // DLProgress
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(410, 113);
            this.Controls.Add(this.LDL);
            this.Controls.Add(this.PB);
            this.Name = "DLProgress";
            this.Text = "DLProgress";
            this.Load += new System.EventHandler(this.DLProgress_Load_1);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ProgressBar PB;
        private System.Windows.Forms.Label LDL;
    }
}