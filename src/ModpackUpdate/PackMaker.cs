﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ModpackUpdate
{
    public partial class PackMaker : Form
    {
        //Init window and list
        public PackMaker()
        {
            InitializeComponent();
            LBMURLs.DataSource = URLs;
        }
        //Init vars for
        int FTot = 0;
        List<string> URLs = new List<string>();
        //Add function
        private void BAdd_Click(object sender, EventArgs e)
        {
            Form Add = new URLAdd();
            Add.ShowDialog();
            if (URLAdd.url != "")
            {
                BSave.Enabled = true;
                URLs.Add(URLAdd.url);
                RefreshL();
            }
        }
        //Refresh function becaus fewer lines lol
        private void RefreshL()
        {
            LBMURLs.DataSource = null;
            LBMURLs.DataSource = URLs;
        }
        //Remove function
        private void BRemove_Click(object sender, EventArgs e)
        {
            if (LBMURLs.SelectedItems != null)
            {
                URLs.Remove(LBMURLs.SelectedItem.ToString());
                RefreshL();
                if (LBMURLs.Items == null)
                {
                    BSave.Enabled = false;
                }
            }
        }
        //Clear function
        private void BClear_Click(object sender, EventArgs e)
        {
            if (URLs != null)
            {
                Form promp = new ConfirmClear();
                promp.ShowDialog();
                if (ConfirmClear.prom == true)
                {
                    URLs.Clear();
                    RefreshL();
                }
            }
        }
        //Load mod file function
        private void BLoad_Click(object sender, EventArgs e)
        {
            if (URLs != null)
            {
                Form promp = new ConfirmClear();
                promp.ShowDialog();
                if (ConfirmClear.prom == true)
                {
                    openFileDialog1.ShowDialog();
                    string filetarg = openFileDialog1.FileName;
                    if (filetarg.Contains(".m"))
                    {
                        LFDIR.Text = filetarg;
                        FTot = 0;
                        string[] lines = System.IO.File.ReadAllLines(filetarg);
                        foreach (string i in lines)
                        {
                            URLs.Add(i);
                            if (i.Contains(".jar") || i.Contains(".litemod"))
                            {
                                FTot += 1;
                            }
                        }
                        LTMods.Text = FTot.ToString();
                        RefreshL();
                        BSave.Enabled = true;
                    }
                    else
                    {
                        Err.EM("That is an invalid filetype", "Input Error");
                    }
                }
            }
        }
        //Save mod list to file
        private void BSave_Click(object sender, EventArgs e)
        {
            if (SFDList.ShowDialog() == DialogResult.OK)
            {
                StreamWriter writer = new StreamWriter(SFDList.OpenFile());
                foreach(string i in URLs)
                {
                    writer.WriteLine(i);
                }
                writer.Dispose();
                writer.Close();
            }
        }
        //Close Window
        private void BClose_Click(object sender, EventArgs e)
        {
            if(URLs != null)
            {
                Form promp = new ConfirmClear();
                promp.ShowDialog();
                if (ConfirmClear.prom == true)
                {
                    Close();
                }
                else
                {
                    RefreshL();
                }
            }
            else
            {
                Close();
            }
        }
    }
}
