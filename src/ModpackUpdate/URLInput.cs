﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ModpackUpdate
{
    public partial class URLInput : Form
    {
        public static string LinkType = "";

        public URLInput()
        {
            InitializeComponent();
        }

        private void RBFile_CheckedChanged(object sender, EventArgs e)
        {
            BBrowse.Enabled = true;
            if (RBURL.Checked == true)
            {
                TBLocation.Text = "";
            }
        }

        private void RBURL_CheckedChanged(object sender, EventArgs e)
        {
            BBrowse.Enabled = false;
            if (RBFile.Checked == true)
            {
                TBLocation.Text = "";
            }
        }

        private void BBrowse_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            string file = openFileDialog1.FileName;
            TBLocation.Text = file;
        }

        private void BCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void BAccept_Click(object sender, EventArgs e)
        {
            string loc = TBLocation.Text;
            Close();
            if (RBURL.Checked == true && Err.IsURL(loc) == true)
            {
                DLMan.DLURL = TBLocation.Text;
                LinkType = "URL";
            }
            else if (RBFile.Checked == true && Err.IsDIR(loc) == true)
            {
                DLMan.DLURL = TBLocation.Text;
                LinkType = "File";
            }
            else if (RBFile.Checked == false && RBURL.Checked == false)
            {
                Err.EM("Please Select a type to load", "Input Error");
            }
            else if (Err.IsNonNullString(loc) != true)
            {
                Err.EM("URL/File cannot be blank", "Input Error");
            }
        }
    }
}
