﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

/*
    Program:        Universal Modpack Manager  
    Author:         Nac/Nalal/Host
    Organization:   FTC
    Init Date:      9/Jan/2016
    
*/

namespace ModpackUpdate
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
        }
        private void Main_Load(object sender, EventArgs e)
        {
            //Initialize directories on program launch
            FileIO.InitDirs();
        }
        //Help dialog
        private void BHelp_Click(object sender, EventArgs e)
        {
            Form help = new Help();
            help.Show();
        }
        //Locate .minecraft folder and load to FileIO
        private void BMineDIR_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.ShowDialog();
            string dir = folderBrowserDialog1.SelectedPath;
            if (Err.IsNonNullString(dir) == true && Err.IsDIRExists(System.IO.Path.Combine(dir, "resourcepacks")) == true)
            {
                TBMineDIR.Text = dir;
            }
            else if(Err.IsNonNullString(dir) != true || Err.IsDIRExists(System.IO.Path.Combine(dir, "resourcepacks")) != true)
            {
                MessageBox.Show("That's not a valid Minecraft DIR, please run minecraft at least once to initialize game files","Input Error");
            }
        }
        //Load modpack location from URL or File
        private void BLoad_Click(object sender, EventArgs e)
        {
            Form input = new URLInput();
            input.ShowDialog();
            if(Err.IsNonNullString(DLMan.DLURL) == true)
            {
                LName.Text = DLMan.DLURL;
            }
        }
        //Install loaded modpack to .minecraft folder
        private void BUp_Click(object sender, EventArgs e)
        {
            FileIO.ReadListDL();
        }
        //Unload loaded data
        private void BUnload_Click(object sender, EventArgs e)
        {
            string unload = "*NOT LOADED*";
            DLMan.DLURL = "";
            LDIR.Text = unload;
            LName.Text = unload;
        }
        //It does the close thing
        private void BClose_Click(object sender, EventArgs e)
        {
            Close();
        }
        //Modifies existing modpack file and edit it
        private void BMod_Click(object sender, EventArgs e)
        {
            Form Packman = new PackMaker();
            Packman.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FileIO.MineDIR = TBMineDIR.Text;
            LDIR.Text = TBMineDIR.Text;
        }

        private void BILLoader_Click(object sender, EventArgs e)
        {
            Err.inc();
        }

        private void BIForge_Click(object sender, EventArgs e)
        {
            Err.inc();
        }
    }
}
