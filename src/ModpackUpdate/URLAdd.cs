﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ModpackUpdate
{
    public partial class URLAdd : Form
    {
        public static string url = "";
        public URLAdd()
        {
            InitializeComponent();
            url = "";
        }
        private void BAccept_Click(object sender, EventArgs e)
        {
            if (Err.IsNonNullString(TBURL.Text) && Err.IsURL(TBURL.Text) == true)
            {
                url = TBURL.Text;
                Close();
            }
            else
            {
                Err.EM("Invalid URL format, please include the HTTP section of the URL","Input Error");
            }
        }

        private void BCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
