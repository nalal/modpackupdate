﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ModpackUpdate
{
    public class Err
    {
        //Error message
        public static void EM(string text, string type)
        {
            MessageBox.Show(text, type);
        }
        //Incomplete message
        public static void inc()
        {
            EM("This function is still being worked on", "UNDER CONSTRUCTION");
        }
        //Check string to see if it's null
        public static bool IsNonNullString(string str)
        {
            bool res = false;
            if (str != "" && str != null)
            {
                res = true;
            }
            return res;
        }
        //Check if list is null
        public static bool IsNonNullList(List<string> ls)
        {
            bool res = IsNonNullString(ls[0]);
            return res;
        }
        public static bool IsNonNullArray(string[] ls)
        {
            bool res = IsNonNullString(ls[0]);
            return res;
        }
        //Check string to see if it's a URL
        public static bool IsURL(string url)
        {
            bool res = false;
            if (IsNonNullString(url) == true)
            {
                if (url.Contains("http") == true && url.Contains(".") == true)
                {
                    res = true;
                }
            }
            return res;
        }
        //Check string to see if it's a DIR
        public static bool IsDIR(string dir)
        {
            bool res = false;
            if (IsNonNullString(dir) == true)
            {
                if (dir.Contains(":") == true || dir.Contains(".mdp") == true)
                {
                    res = true;
                }
            }
            return res;
        }
        //Check if file exists
        public static bool IsFileExists(string filetarg)
        {
            bool res = false;
            if (System.IO.File.Exists(filetarg) == true)
            {
                res = true;
            }
            return res;
        }
        public static bool IsDIRExists(string DIRtarg)
        {
            bool res = false;
            if (System.IO.Directory.Exists(DIRtarg) == true)
            {
                res = true;
            }
            return res;
        }
    }
}
