﻿namespace ModpackUpdate
{
    partial class PackMaker
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LBMURLs = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.BClear = new System.Windows.Forms.Button();
            this.BClose = new System.Windows.Forms.Button();
            this.BAdd = new System.Windows.Forms.Button();
            this.BRemove = new System.Windows.Forms.Button();
            this.BSave = new System.Windows.Forms.Button();
            this.BLoad = new System.Windows.Forms.Button();
            this.SFDList = new System.Windows.Forms.SaveFileDialog();
            this.label2 = new System.Windows.Forms.Label();
            this.LFDIR = new System.Windows.Forms.Label();
            this.LTMods = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.SuspendLayout();
            // 
            // LBMURLs
            // 
            this.LBMURLs.FormattingEnabled = true;
            this.LBMURLs.Location = new System.Drawing.Point(23, 81);
            this.LBMURLs.Name = "LBMURLs";
            this.LBMURLs.Size = new System.Drawing.Size(545, 290);
            this.LBMURLs.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 65);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Mod URLs:";
            // 
            // BClear
            // 
            this.BClear.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.BClear.Location = new System.Drawing.Point(574, 139);
            this.BClear.Name = "BClear";
            this.BClear.Size = new System.Drawing.Size(75, 23);
            this.BClear.TabIndex = 2;
            this.BClear.Text = "Clear";
            this.BClear.UseVisualStyleBackColor = true;
            this.BClear.Click += new System.EventHandler(this.BClear_Click);
            // 
            // BClose
            // 
            this.BClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BClose.Location = new System.Drawing.Point(574, 348);
            this.BClose.Name = "BClose";
            this.BClose.Size = new System.Drawing.Size(75, 23);
            this.BClose.TabIndex = 3;
            this.BClose.Text = "Close";
            this.BClose.UseVisualStyleBackColor = true;
            this.BClose.Click += new System.EventHandler(this.BClose_Click);
            // 
            // BAdd
            // 
            this.BAdd.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.BAdd.Location = new System.Drawing.Point(574, 81);
            this.BAdd.Name = "BAdd";
            this.BAdd.Size = new System.Drawing.Size(75, 23);
            this.BAdd.TabIndex = 4;
            this.BAdd.Text = "Add";
            this.BAdd.UseVisualStyleBackColor = true;
            this.BAdd.Click += new System.EventHandler(this.BAdd_Click);
            // 
            // BRemove
            // 
            this.BRemove.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.BRemove.Location = new System.Drawing.Point(574, 110);
            this.BRemove.Name = "BRemove";
            this.BRemove.Size = new System.Drawing.Size(75, 23);
            this.BRemove.TabIndex = 5;
            this.BRemove.Text = "Remove";
            this.BRemove.UseVisualStyleBackColor = true;
            this.BRemove.Click += new System.EventHandler(this.BRemove_Click);
            // 
            // BSave
            // 
            this.BSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BSave.Enabled = false;
            this.BSave.Location = new System.Drawing.Point(574, 53);
            this.BSave.Name = "BSave";
            this.BSave.Size = new System.Drawing.Size(75, 23);
            this.BSave.TabIndex = 6;
            this.BSave.Text = "Save List";
            this.BSave.UseVisualStyleBackColor = true;
            this.BSave.Click += new System.EventHandler(this.BSave_Click);
            // 
            // BLoad
            // 
            this.BLoad.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BLoad.Location = new System.Drawing.Point(574, 22);
            this.BLoad.Name = "BLoad";
            this.BLoad.Size = new System.Drawing.Size(75, 23);
            this.BLoad.TabIndex = 7;
            this.BLoad.Text = "Load List";
            this.BLoad.UseVisualStyleBackColor = true;
            this.BLoad.Click += new System.EventHandler(this.BLoad_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Mod File:";
            // 
            // LFDIR
            // 
            this.LFDIR.AutoSize = true;
            this.LFDIR.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LFDIR.Location = new System.Drawing.Point(23, 22);
            this.LFDIR.Name = "LFDIR";
            this.LFDIR.Size = new System.Drawing.Size(87, 15);
            this.LFDIR.TabIndex = 9;
            this.LFDIR.Text = "*NOT LOADED*";
            // 
            // LTMods
            // 
            this.LTMods.AutoSize = true;
            this.LTMods.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LTMods.Location = new System.Drawing.Point(23, 50);
            this.LTMods.Name = "LTMods";
            this.LTMods.Size = new System.Drawing.Size(87, 15);
            this.LTMods.TabIndex = 11;
            this.LTMods.Text = "*NOT LOADED*";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 37);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Total Mods:";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // PackMaker
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(661, 385);
            this.Controls.Add(this.LTMods);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.LFDIR);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.BLoad);
            this.Controls.Add(this.BSave);
            this.Controls.Add(this.BRemove);
            this.Controls.Add(this.BAdd);
            this.Controls.Add(this.BClose);
            this.Controls.Add(this.BClear);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.LBMURLs);
            this.Name = "PackMaker";
            this.Text = "Pack Maker";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox LBMURLs;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button BClear;
        private System.Windows.Forms.Button BClose;
        private System.Windows.Forms.Button BAdd;
        private System.Windows.Forms.Button BRemove;
        private System.Windows.Forms.Button BSave;
        private System.Windows.Forms.Button BLoad;
        private System.Windows.Forms.SaveFileDialog SFDList;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label LFDIR;
        private System.Windows.Forms.Label LTMods;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
    }
}