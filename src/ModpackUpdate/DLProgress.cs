﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ModpackUpdate
{
    public partial class DLProgress : Form
    {
        public DLProgress()
        {
            InitializeComponent();
        }
        private void DL()
        {
            string DLLoc = "";
            if (DLMan.FileName.Contains(".m"))
            {
                DLLoc = FileIO.LocalDP;
            }
            if (DLMan.FileName.Contains(".jar") || DLMan.FileName.Contains(".litemod"))
            {
                DLLoc = FileIO.LocalDM;
            }
            if (DLMan.FileName.Contains(".txt") || DLMan.FileName.Contains(".cfg") || DLMan.FileName.Contains(".properties"))
            {
                DLLoc = FileIO.LocalDC;
            }
            else if(DLMan.FileName.Contains(".m") != true && DLMan.FileName.Contains(".txt") != true && DLMan.FileName.Contains(".cfg") != true && DLMan.FileName.Contains(".properties") != true && DLMan.FileName.Contains(".jar") != true && DLMan.FileName.Contains(".litemod") != true)
            {
                MessageBox.Show(DLMan.FileName + " Is not a valid file type, skipping.", "Warning");
                Close();
            }
            using (WebClient wc = new WebClient())
            {
                wc.DownloadProgressChanged += wc_DownloadProgressChanged;
                wc.DownloadFileCompleted += wc_DownloadFileCompleted;
                wc.DownloadFileAsync(new Uri(DLMan.DLTarg), System.IO.Path.Combine(DLLoc, DLMan.FileName));
            }
            Close();
        }
        //Track DL progress
        private void wc_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            PB.Value = e.ProgressPercentage;
        }
        //Catch DL end
        private void wc_DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
        {
            PB.Value = 0;

            if (e.Cancelled)
            {
                MessageBox.Show("The download has been cancelled");
                return;
            }

            if (e.Error != null)
            {
                MessageBox.Show("The following error occured durring download.\n" + e.Error.ToString(),"Download Error");
                return;
            }
            Close();
        }

        private void DLProgress_Load_1(object sender, EventArgs e)
        {
            //Init form with Download opperation
            LDL.Text = "Downloading: " + DLMan.FileName + " From: " + DLMan.DLTarg;
            DL();
        }
    }
}
