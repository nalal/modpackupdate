﻿namespace ModpackUpdate
{
    partial class URLAdd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TBURL = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.BCancel = new System.Windows.Forms.Button();
            this.BAccept = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // TBURL
            // 
            this.TBURL.Location = new System.Drawing.Point(12, 25);
            this.TBURL.Name = "TBURL";
            this.TBURL.Size = new System.Drawing.Size(373, 20);
            this.TBURL.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "URL:";
            // 
            // BCancel
            // 
            this.BCancel.Location = new System.Drawing.Point(310, 51);
            this.BCancel.Name = "BCancel";
            this.BCancel.Size = new System.Drawing.Size(75, 23);
            this.BCancel.TabIndex = 2;
            this.BCancel.Text = "Cancel";
            this.BCancel.UseVisualStyleBackColor = true;
            this.BCancel.Click += new System.EventHandler(this.BCancel_Click);
            // 
            // BAccept
            // 
            this.BAccept.Location = new System.Drawing.Point(12, 51);
            this.BAccept.Name = "BAccept";
            this.BAccept.Size = new System.Drawing.Size(75, 23);
            this.BAccept.TabIndex = 3;
            this.BAccept.Text = "Accept";
            this.BAccept.UseVisualStyleBackColor = true;
            this.BAccept.Click += new System.EventHandler(this.BAccept_Click);
            // 
            // URLAdd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(399, 96);
            this.Controls.Add(this.BAccept);
            this.Controls.Add(this.BCancel);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TBURL);
            this.Name = "URLAdd";
            this.Text = "URLAdd";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TBURL;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button BCancel;
        private System.Windows.Forms.Button BAccept;
    }
}