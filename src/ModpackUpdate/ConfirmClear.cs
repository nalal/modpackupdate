﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ModpackUpdate
{
    public partial class ConfirmClear : Form
    {
        public ConfirmClear()
        {
            InitializeComponent();
        }
        public static bool prom = false;
        private void BAccept_Click(object sender, EventArgs e)
        {
            prom = true;
            Close();
        }

        private void BCancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
