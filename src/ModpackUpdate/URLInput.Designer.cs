﻿namespace ModpackUpdate
{
    partial class URLInput
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TBLocation = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.RBURL = new System.Windows.Forms.RadioButton();
            this.RBFile = new System.Windows.Forms.RadioButton();
            this.BBrowse = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.BAccept = new System.Windows.Forms.Button();
            this.BCancel = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // TBLocation
            // 
            this.TBLocation.Location = new System.Drawing.Point(6, 32);
            this.TBLocation.Name = "TBLocation";
            this.TBLocation.Size = new System.Drawing.Size(245, 20);
            this.TBLocation.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "URL/Packfile:";
            // 
            // RBURL
            // 
            this.RBURL.AutoSize = true;
            this.RBURL.Location = new System.Drawing.Point(6, 58);
            this.RBURL.Name = "RBURL";
            this.RBURL.Size = new System.Drawing.Size(47, 17);
            this.RBURL.TabIndex = 2;
            this.RBURL.TabStop = true;
            this.RBURL.Text = "URL";
            this.RBURL.UseVisualStyleBackColor = true;
            this.RBURL.CheckedChanged += new System.EventHandler(this.RBURL_CheckedChanged);
            // 
            // RBFile
            // 
            this.RBFile.AutoSize = true;
            this.RBFile.Location = new System.Drawing.Point(6, 81);
            this.RBFile.Name = "RBFile";
            this.RBFile.Size = new System.Drawing.Size(41, 17);
            this.RBFile.TabIndex = 3;
            this.RBFile.TabStop = true;
            this.RBFile.Text = "File";
            this.RBFile.UseVisualStyleBackColor = true;
            this.RBFile.CheckedChanged += new System.EventHandler(this.RBFile_CheckedChanged);
            // 
            // BBrowse
            // 
            this.BBrowse.Enabled = false;
            this.BBrowse.Location = new System.Drawing.Point(176, 78);
            this.BBrowse.Name = "BBrowse";
            this.BBrowse.Size = new System.Drawing.Size(75, 23);
            this.BBrowse.TabIndex = 4;
            this.BBrowse.Text = "Browse";
            this.BBrowse.UseVisualStyleBackColor = true;
            this.BBrowse.Click += new System.EventHandler(this.BBrowse_Click);
            // 
            // BAccept
            // 
            this.BAccept.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.BAccept.Location = new System.Drawing.Point(12, 126);
            this.BAccept.Name = "BAccept";
            this.BAccept.Size = new System.Drawing.Size(75, 23);
            this.BAccept.TabIndex = 5;
            this.BAccept.Text = "Accept";
            this.BAccept.UseVisualStyleBackColor = true;
            this.BAccept.Click += new System.EventHandler(this.BAccept_Click);
            // 
            // BCancel
            // 
            this.BCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.BCancel.Location = new System.Drawing.Point(93, 126);
            this.BCancel.Name = "BCancel";
            this.BCancel.Size = new System.Drawing.Size(75, 23);
            this.BCancel.TabIndex = 6;
            this.BCancel.Text = "Cancel";
            this.BCancel.UseVisualStyleBackColor = true;
            this.BCancel.Click += new System.EventHandler(this.BCancel_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.TBLocation);
            this.groupBox1.Controls.Add(this.RBURL);
            this.groupBox1.Controls.Add(this.BBrowse);
            this.groupBox1.Controls.Add(this.RBFile);
            this.groupBox1.Location = new System.Drawing.Point(12, 9);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(263, 111);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Please input your modpack";
            // 
            // URLInput
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(288, 161);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.BCancel);
            this.Controls.Add(this.BAccept);
            this.Name = "URLInput";
            this.Text = "Pack Input";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox TBLocation;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton RBURL;
        private System.Windows.Forms.RadioButton RBFile;
        private System.Windows.Forms.Button BBrowse;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button BAccept;
        private System.Windows.Forms.Button BCancel;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}