﻿namespace ModpackUpdate
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BUp = new System.Windows.Forms.Button();
            this.BLoad = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.LName = new System.Windows.Forms.Label();
            this.LDIR = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.BHelp = new System.Windows.Forms.Button();
            this.BUnload = new System.Windows.Forms.Button();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.BClose = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.TBMineDIR = new System.Windows.Forms.TextBox();
            this.BMineDIR = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.BMod = new System.Windows.Forms.Button();
            this.BIForge = new System.Windows.Forms.Button();
            this.BILLoader = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // BUp
            // 
            this.BUp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.BUp.Location = new System.Drawing.Point(12, 212);
            this.BUp.Name = "BUp";
            this.BUp.Size = new System.Drawing.Size(97, 23);
            this.BUp.TabIndex = 1;
            this.BUp.Text = "Update";
            this.BUp.UseVisualStyleBackColor = true;
            this.BUp.Click += new System.EventHandler(this.BUp_Click);
            // 
            // BLoad
            // 
            this.BLoad.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.BLoad.Location = new System.Drawing.Point(12, 125);
            this.BLoad.Name = "BLoad";
            this.BLoad.Size = new System.Drawing.Size(97, 23);
            this.BLoad.TabIndex = 4;
            this.BLoad.Text = "Load Pack";
            this.BLoad.UseVisualStyleBackColor = true;
            this.BLoad.Click += new System.EventHandler(this.BLoad_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Pack Location:";
            // 
            // LName
            // 
            this.LName.AutoSize = true;
            this.LName.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LName.Location = new System.Drawing.Point(22, 22);
            this.LName.Name = "LName";
            this.LName.Size = new System.Drawing.Size(87, 15);
            this.LName.TabIndex = 7;
            this.LName.Text = "*NOT LOADED*";
            // 
            // LDIR
            // 
            this.LDIR.AutoSize = true;
            this.LDIR.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.LDIR.Location = new System.Drawing.Point(22, 50);
            this.LDIR.Name = "LDIR";
            this.LDIR.Size = new System.Drawing.Size(87, 15);
            this.LDIR.TabIndex = 11;
            this.LDIR.Text = "*NOT LOADED*";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 37);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(76, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Minecraft DIR:";
            // 
            // BHelp
            // 
            this.BHelp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.BHelp.Location = new System.Drawing.Point(12, 96);
            this.BHelp.Name = "BHelp";
            this.BHelp.Size = new System.Drawing.Size(97, 23);
            this.BHelp.TabIndex = 13;
            this.BHelp.Text = "Help";
            this.BHelp.UseVisualStyleBackColor = true;
            this.BHelp.Click += new System.EventHandler(this.BHelp_Click);
            // 
            // BUnload
            // 
            this.BUnload.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.BUnload.Location = new System.Drawing.Point(12, 183);
            this.BUnload.Name = "BUnload";
            this.BUnload.Size = new System.Drawing.Size(97, 23);
            this.BUnload.TabIndex = 14;
            this.BUnload.Text = "Unload";
            this.BUnload.UseVisualStyleBackColor = true;
            this.BUnload.Click += new System.EventHandler(this.BUnload_Click);
            // 
            // BClose
            // 
            this.BClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BClose.Location = new System.Drawing.Point(271, 211);
            this.BClose.Name = "BClose";
            this.BClose.Size = new System.Drawing.Size(97, 23);
            this.BClose.TabIndex = 16;
            this.BClose.Text = "Close";
            this.BClose.UseVisualStyleBackColor = true;
            this.BClose.Click += new System.EventHandler(this.BClose_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(227, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(141, 13);
            this.label2.TabIndex = 17;
            this.label2.Text = "[UNDER CONSTRUCTION]";
            // 
            // TBMineDIR
            // 
            this.TBMineDIR.Location = new System.Drawing.Point(115, 156);
            this.TBMineDIR.Name = "TBMineDIR";
            this.TBMineDIR.Size = new System.Drawing.Size(150, 20);
            this.TBMineDIR.TabIndex = 18;
            // 
            // BMineDIR
            // 
            this.BMineDIR.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.BMineDIR.Location = new System.Drawing.Point(271, 154);
            this.BMineDIR.Name = "BMineDIR";
            this.BMineDIR.Size = new System.Drawing.Size(97, 23);
            this.BMineDIR.TabIndex = 19;
            this.BMineDIR.Text = "Browse";
            this.BMineDIR.UseVisualStyleBackColor = true;
            this.BMineDIR.Click += new System.EventHandler(this.BMineDIR_Click);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button1.Location = new System.Drawing.Point(12, 154);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(97, 23);
            this.button1.TabIndex = 21;
            this.button1.Text = "Set .minecraft";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // BMod
            // 
            this.BMod.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BMod.Location = new System.Drawing.Point(271, 182);
            this.BMod.Name = "BMod";
            this.BMod.Size = new System.Drawing.Size(97, 23);
            this.BMod.TabIndex = 15;
            this.BMod.Text = "Mod Packer";
            this.BMod.UseVisualStyleBackColor = true;
            this.BMod.Click += new System.EventHandler(this.BMod_Click);
            // 
            // BIForge
            // 
            this.BIForge.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.BIForge.Location = new System.Drawing.Point(271, 125);
            this.BIForge.Name = "BIForge";
            this.BIForge.Size = new System.Drawing.Size(97, 23);
            this.BIForge.TabIndex = 22;
            this.BIForge.Text = "Install Forge";
            this.BIForge.UseVisualStyleBackColor = true;
            this.BIForge.Click += new System.EventHandler(this.BIForge_Click);
            // 
            // BILLoader
            // 
            this.BILLoader.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.BILLoader.Location = new System.Drawing.Point(271, 96);
            this.BILLoader.Name = "BILLoader";
            this.BILLoader.Size = new System.Drawing.Size(97, 23);
            this.BILLoader.TabIndex = 23;
            this.BILLoader.Text = "Install LiteLoader";
            this.BILLoader.UseVisualStyleBackColor = true;
            this.BILLoader.Click += new System.EventHandler(this.BILLoader_Click);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(380, 246);
            this.Controls.Add(this.BILLoader);
            this.Controls.Add(this.BIForge);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.BMineDIR);
            this.Controls.Add(this.TBMineDIR);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.BClose);
            this.Controls.Add(this.BMod);
            this.Controls.Add(this.BUnload);
            this.Controls.Add(this.BHelp);
            this.Controls.Add(this.LDIR);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.LName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.BLoad);
            this.Controls.Add(this.BUp);
            this.Name = "Main";
            this.Text = "Modpacker";
            this.Load += new System.EventHandler(this.Main_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button BUp;
        private System.Windows.Forms.Button BLoad;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label LName;
        private System.Windows.Forms.Label LDIR;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button BHelp;
        private System.Windows.Forms.Button BUnload;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Button BClose;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TBMineDIR;
        private System.Windows.Forms.Button BMineDIR;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button BMod;
        private System.Windows.Forms.Button BIForge;
        private System.Windows.Forms.Button BILLoader;
    }
}

